EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title "Motor Control Board - MCU"
Date "2020-08-30"
Rev "1"
Comp "Sukriansyah"
Comment1 "Technical Test Electronic Engineer eFishery"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:C C201
U 1 1 5F4A01A3
P 5825 2225
F 0 "C201" V 5573 2225 50  0000 C CNN
F 1 "0.1uF" V 5664 2225 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5863 2075 50  0001 C CNN
F 3 "~" H 5825 2225 50  0001 C CNN
	1    5825 2225
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0202
U 1 1 5F4A0805
P 6025 2275
F 0 "#PWR0202" H 6025 2025 50  0001 C CNN
F 1 "GND" H 6025 2125 50  0000 C CNN
F 2 "" H 6025 2275 50  0001 C CNN
F 3 "" H 6025 2275 50  0001 C CNN
	1    6025 2275
	1    0    0    -1  
$EndComp
Wire Wire Line
	5675 2225 5625 2225
Wire Wire Line
	5625 2225 5625 2450
Wire Wire Line
	5975 2225 6025 2225
Wire Wire Line
	6025 2225 6025 2275
Wire Wire Line
	5525 2450 5525 2225
Wire Wire Line
	5525 2225 5625 2225
Connection ~ 5625 2225
Text HLabel 5525 2175 1    50   Input ~ 0
VCC
Wire Wire Line
	5525 2175 5525 2225
Connection ~ 5525 2225
Text HLabel 4875 2750 0    50   Input ~ 0
AREF
Wire Wire Line
	4875 2750 4925 2750
$Comp
L Device:Crystal Y201
U 1 1 5F4A4A9E
P 7575 3800
F 0 "Y201" H 7575 4068 50  0000 C CNN
F 1 "16MHz" H 7575 3977 50  0000 C CNN
F 2 "Crystal:Resonator-2Pin_W6.0mm_H3.0mm" H 7575 3800 50  0001 C CNN
F 3 "~" H 7575 3800 50  0001 C CNN
	1    7575 3800
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW201
U 1 1 5F4A52A0
P 6775 4500
F 0 "SW201" V 6729 4648 50  0000 L CNN
F 1 "RESET" V 6820 4648 50  0000 L CNN
F 2 "Button_Switch_SMD:SW_Push_1P1T_NO_6x6mm_H9.5mm" H 6775 4700 50  0001 C CNN
F 3 "~" H 6775 4700 50  0001 C CNN
	1    6775 4500
	0    1    1    0   
$EndComp
$Comp
L Device:R R201
U 1 1 5F4A5DBF
P 6775 4050
F 0 "R201" V 6850 4050 50  0000 C CNN
F 1 "10K" V 6775 4050 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6705 4050 50  0001 C CNN
F 3 "~" H 6775 4050 50  0001 C CNN
	1    6775 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6775 4200 6775 4250
$Comp
L power:GND #PWR0203
U 1 1 5F4A7A94
P 6775 4750
F 0 "#PWR0203" H 6775 4500 50  0001 C CNN
F 1 "GND" H 6775 4600 50  0000 C CNN
F 2 "" H 6775 4750 50  0001 C CNN
F 3 "" H 6775 4750 50  0001 C CNN
	1    6775 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6775 4750 6775 4700
Wire Wire Line
	6125 4250 6775 4250
Connection ~ 6775 4250
Wire Wire Line
	6775 4250 6775 4300
Text HLabel 6825 4250 2    50   Input ~ 0
RESET
Wire Wire Line
	6825 4250 6775 4250
Text Label 6775 3750 3    50   ~ 0
VCC
Wire Wire Line
	6775 3750 6775 3900
$Comp
L Device:C C202
U 1 1 5F4AAA84
P 7325 4050
F 0 "C202" H 7440 4096 50  0000 L CNN
F 1 "20pF" H 7440 4005 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7363 3900 50  0001 C CNN
F 3 "~" H 7325 4050 50  0001 C CNN
	1    7325 4050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C203
U 1 1 5F4AC18D
P 7825 4050
F 0 "C203" H 7940 4096 50  0000 L CNN
F 1 "20pF" H 7940 4005 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7863 3900 50  0001 C CNN
F 3 "~" H 7825 4050 50  0001 C CNN
	1    7825 4050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0204
U 1 1 5F4ACC06
P 7325 4250
F 0 "#PWR0204" H 7325 4000 50  0001 C CNN
F 1 "GND" H 7325 4100 50  0000 C CNN
F 2 "" H 7325 4250 50  0001 C CNN
F 3 "" H 7325 4250 50  0001 C CNN
	1    7325 4250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0205
U 1 1 5F4AD15B
P 7825 4250
F 0 "#PWR0205" H 7825 4000 50  0001 C CNN
F 1 "GND" H 7825 4100 50  0000 C CNN
F 2 "" H 7825 4250 50  0001 C CNN
F 3 "" H 7825 4250 50  0001 C CNN
	1    7825 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7825 4250 7825 4200
Wire Wire Line
	7825 3800 7725 3800
Wire Wire Line
	7325 3800 7325 3900
Wire Wire Line
	7425 3800 7325 3800
Wire Wire Line
	7325 4200 7325 4250
Wire Wire Line
	7825 3900 7825 3800
Wire Wire Line
	6125 3350 7825 3350
Wire Wire Line
	7825 3350 7825 3800
Connection ~ 7825 3800
Wire Wire Line
	6125 3450 7325 3450
Wire Wire Line
	7325 3450 7325 3800
Connection ~ 7325 3800
$Comp
L power:GND #PWR0201
U 1 1 5F4B1466
P 5525 5500
F 0 "#PWR0201" H 5525 5250 50  0001 C CNN
F 1 "GND" H 5525 5350 50  0000 C CNN
F 2 "" H 5525 5500 50  0001 C CNN
F 3 "" H 5525 5500 50  0001 C CNN
	1    5525 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5525 5500 5525 5450
Text HLabel 6175 2750 2    50   Input ~ 0
PB0
Text HLabel 6175 2850 2    50   Input ~ 0
PB1
Wire Wire Line
	6125 2750 6175 2750
Wire Wire Line
	6125 2850 6175 2850
Text HLabel 6175 2950 2    50   Input ~ 0
PB2
Text HLabel 6175 3050 2    50   Input ~ 0
PB3
Wire Wire Line
	6125 2950 6175 2950
Wire Wire Line
	6125 3050 6175 3050
Text HLabel 6175 3150 2    50   Input ~ 0
PB4
Text HLabel 6175 3250 2    50   Input ~ 0
PB5
Wire Wire Line
	6125 3150 6175 3150
Wire Wire Line
	6125 3250 6175 3250
Text HLabel 7875 3350 2    50   Input ~ 0
PB6_XTAL1
Text HLabel 7875 3450 2    50   Input ~ 0
PB7_XTAL2
Wire Wire Line
	7825 3350 7875 3350
Connection ~ 7825 3350
Wire Wire Line
	7325 3450 7875 3450
Connection ~ 7325 3450
Text HLabel 6175 3650 2    50   Input ~ 0
PC0
Text HLabel 6175 3750 2    50   Input ~ 0
PC1
Wire Wire Line
	6125 3650 6175 3650
Wire Wire Line
	6125 3750 6175 3750
Text HLabel 6175 3850 2    50   Input ~ 0
PC2
Text HLabel 6175 3950 2    50   Input ~ 0
PC3
Wire Wire Line
	6125 3850 6175 3850
Wire Wire Line
	6125 3950 6175 3950
Text HLabel 6175 4050 2    50   Input ~ 0
PC4
Text HLabel 6175 4150 2    50   Input ~ 0
PC5
Wire Wire Line
	6125 4050 6175 4050
Wire Wire Line
	6125 4150 6175 4150
Text HLabel 6175 4450 2    50   Input ~ 0
PD0
Text HLabel 6175 4550 2    50   Input ~ 0
PD1
Wire Wire Line
	6125 4450 6175 4450
Wire Wire Line
	6125 4550 6175 4550
Text HLabel 6175 4650 2    50   Input ~ 0
PD2
Text HLabel 6175 4750 2    50   Input ~ 0
PD3
Wire Wire Line
	6125 4650 6175 4650
Wire Wire Line
	6125 4750 6175 4750
Text HLabel 6175 4850 2    50   Input ~ 0
PD4
Text HLabel 6175 4950 2    50   Input ~ 0
PD5
Wire Wire Line
	6125 4850 6175 4850
Wire Wire Line
	6125 4950 6175 4950
Text HLabel 6175 5050 2    50   Input ~ 0
PD6
Text HLabel 6175 5150 2    50   Input ~ 0
PD7
Wire Wire Line
	6125 5050 6175 5050
Wire Wire Line
	6125 5150 6175 5150
$Comp
L MCU_Microchip_ATmega:ATmega328-AU U201
U 1 1 5F8CE113
P 5525 3950
F 0 "U201" H 5075 2475 50  0000 C CNN
F 1 "ATmega328-AU" H 5875 2475 50  0000 C CNN
F 2 "Package_QFP:TQFP-32_7x7mm_P0.8mm" H 5525 3950 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/ATmega328_P%20AVR%20MCU%20with%20picoPower%20Technology%20Data%20Sheet%2040001984A.pdf" H 5525 3950 50  0001 C CNN
	1    5525 3950
	1    0    0    -1  
$EndComp
$EndSCHEMATC
